<?php
/**
 * @file comment.tpl.php
 * Renders comments for Hiroshige.
 */
$comment_class = 'comment' . (($comment->new) ? ' comment-new' : '') . 
                 ' ' . $status . ' ' . $zebra;
?>
<div class="<?php print $comment_class; ?>">
  <div class="content">
    <?php if ($comment->new): ?>
      <span class="new"><?php print $new ?></span>
    <?php endif; ?>
    <?php if (!empty($title)) { ?><h3><?php print $title ?></h3><?php } ?>
    <?php if (!empty($picture)) { print $picture; } ?>
    <?php print $content ?>
    <?php if ($signature): ?>
      <div class="user-signature clear-block">
        <?php print $signature ?>
      </div>
    <?php endif; ?>
  </div>
  <div class="comment-meta">
    Comment by <strong><?php print $author; ?></strong> on <?php print $date; ?><br/>
    <?php if ($links) { print $links; } ?>
  </div>
</div>
<div class="clear"></div>