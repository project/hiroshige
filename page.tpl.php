<?php
/**
 * @file page.tpl.php
 * Renders the pages for Hiroshige. The following variables are provided in
 * addition to the default variables.
 *   - hiroshige_classes An associative array containing specific computed CSS
 *     classes to apply to various elements on the page.
 */

// Is the sub-header region area completely empty?
$hiroshige_subhead_empty = (empty($sub_head_left) &&
                            empty($sub_head_center_left) &&
                            empty($sub_head_center_right) &&
                            empty($sub_head_right));

// Is the bottom region area completely empty?
$hiroshige_bottom_empty = (empty($bottom_left) &&
                           empty($bottom_center_left) &&
                           empty($bottom_center_right) &&
                           empty($bottom_right));
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
    <!--[if IE]>
        <?php print phptemplate_get_ie_styles() . "\n"; ?>
    <![endif]-->
  </head>
  <body class="<?php print $body_classes; ?>">
    <div id="header-band" class="clear-block">
        <div id="header" class="container_16">
            <?php if (isset($logo)): ?>
            <a href="<?php print check_url($front_page); ?>" title="<?php print check_plain($site_name); ?>"><img src="<?php print check_url($logo); ?>" alt="<?php print $site_title; ?>" id="logo" /></a>
            <?php endif; ?>
            <div id="title-wrapper">
                <h1><a href="<?php print check_url($front_page); ?>" title="<?php print check_plain($site_name); ?>"><?php print check_plain($site_name); ?></a></h1>
                <?php if (isset($site_slogan)): ?>
                <div id="site-slogan"><?php print check_plain($site_slogan); ?></div>
                <?php endif; ?>
            </div> <!-- /#title-wrapper -->
            <div id="search-top" class="grid_4 omega"><?php if ($search_box) { print $search_box; } ?></div>
        </div> <!-- /#header -->
    </div> <!-- /#header-band -->
    <div id="sub-header-band" class="clear-block">
        <div id="sub-header-container" class="container_16">
            <?php if ($hiroshige_subhead_empty): ?>
            <div id="sub-header" class="grid_16">&nbsp;</div>
            <?php endif; ?>
            <?php if (!empty($sub_head_left)): ?>
            <div id="sub-header-left" class="<?php print $hiroshige_classes['sub_head_left']; ?>">
              <?php print $sub_head_left; ?>
            </div> <!-- /#sub-header-left -->
            <?php endif; ?>
            <?php if (!empty($sub_head_center_left)): ?>
            <div id="sub-header-center-left" class="<?php print $hiroshige_classes['sub_head_center_left']; ?>">
              <?php print $sub_head_center_left; ?>
            </div> <!-- /#sub-header-center-left -->
            <?php endif; ?>
            <?php if (!empty($sub_head_center_right)): ?>
            <div id="sub-header-center-right" class="<?php print $hiroshige_classes['sub_head_center_right']; ?>">
              <?php print $sub_head_center_right; ?>
            </div> <!-- /#sub-header-center-right -->
            <?php endif; ?>
            <?php if (!empty($sub_head_right)): ?>
            <div id="sub-header-right" class="<?php print $hiroshige_classes['sub_head_right']; ?>">
              <?php print $sub_head_right; ?>
            </div> <!-- /#sub-header-right -->
            <?php endif; ?>
        </div>
    </div> <!-- /#sub-header-band -->
    <div id="navigation-band" class="clear-block">
        <div id="primary-links" class="container_16">
            <?php if (isset($primary_links)) : ?>
            <?php print theme('links', $primary_links, array('id' => 'topLinks', 'class' => 'grid_16')) ?>
            <?php endif; ?>
        </div>
    </div> <!-- /#navigation-band -->
    <div id="page-content-band" class="clear-block">
        <div id="content-wrapper" class="container_16">
            <div id="page-content" class="<?php print $hiroshige_classes['content']; ?>">
                <?php print $breadcrumb; ?>
                <?php if (!empty($title)): ?>
                <h2 id="page-title"<?php print $hiroshige_classes['page_title']; ?>><?php print $title; ?></h2>
                <?php endif; ?>
                <?php if (!empty($tabs)):?>
                <div id="tabs-wrapper" class="clear-block">
                    <ul class="tabs primary"><?php print $tabs; ?></ul>
                    <?php if ($tabs2): ?>
                        <ul class="tabs secondary"><?php print $tabs2; ?></ul>
                    <?php endif; ?>
                </div> <!-- /#tabs-wrapper -->
                <div class="clear"></div>
                <?php endif; ?>
                <?php if ($show_messages && $messages): print $messages; endif; ?>
                <?php print $help; ?>
                <div id="content-content" class="clear-block">
                  <?php print $content ?>
                </div> <!-- /#content -->
            </div> <!-- /#page-content -->
            <?php if (!empty($left)): ?>
            <div id="sidebar-left" class="<?php print $hiroshige_classes['left']; ?>">
                <?php print $left; ?>
            </div><!-- /#sidebar-left -->
            <?php endif; ?>
            <?php if (!empty($right)): ?>
            <div id="sidebar-right" class="<?php print $hiroshige_classes['right']; ?>">
              <?php print $right; ?>
            </div><!-- /#sidebar-right -->
            <?php endif; ?>
        </div> <!-- #content-wrapper" -->
    </div> <!-- /#page-content-band -->
    <div id="bottom-bar-band" class="clear-block">
        <div id="bottom" class="container_16 clear-block">
            <?php if ($hiroshige_bottom_empty): ?>
            <div id="bottom-spacer" class="grid_16">&nbsp;</div>
            <?php endif; ?>
            <?php if (!empty($bottom_left)): ?>
            <div id="bottom-left" class="<?php print $hiroshige_classes['bottom_left']; ?>">
              <?php print $bottom_left; ?>
            </div> <!-- /#bottom-left -->
            <?php endif; ?>
            <?php if (!empty($bottom_center_left)): ?>
            <div id="bottom-center-left" class="<?php print $hiroshige_classes['bottom_center_left']; ?>">
              <?php print $bottom_center_left; ?>
            </div> <!-- /#bottom-center-left -->
            <?php endif; ?>
            <?php if (!empty($bottom_center_right)): ?>
            <div id="bottom-center-right" class="<?php print $hiroshige_classes['bottom_center_right']; ?>">
              <?php print $bottom_center_right; ?>
            </div> <!-- /#bottom-center-right -->
            <?php endif; ?>
            <?php if (!empty($bottom_right)): ?>
            <div id="bottom-right" class="<?php print $hiroshige_classes['bottom_right']; ?>">
              <?php print $bottom_right; ?>
            </div> <!-- /#bottom-right -->
            <?php endif; ?>
        </div> <!-- /#bottom -->
    </div> <!-- /#bottom-bar-band -->
    <div id="footer-band" class="clear-block">
      <div id="footer" class="container_16">
        <?php print $footer_message . $footer ?>
      </div>
    </div> <!-- /#footer-band -->
    <?php print $closure; ?>
  </body>
</html>
