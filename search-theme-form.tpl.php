<?php

/**
 * @file search-theme-form.tpl.php
 * Renders the search theme form without the 'Search' button.
 */
?>
<div id="search" class="container-inline">
  <?php print $search['search_theme_form'] . $search['hidden']; ?>
</div>
