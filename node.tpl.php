<?php
/**
 * @file node.tpl.php
 * Overrides node rendering for Hiroshige. The following variables are
 * provided in addition to the default variables.
 *   - hiroshige_node_class - The class list to assign to the div element that
 *     wraps the node.
 *   - hiroshige_node_link - The rendered perma-link for the node.
 *   - hiroshige_posted_by - The rendered authoring, timestamp, and tag 
 *     information.
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $hiroshige_node_class; ?>">
    <?php if ($page == 0): ?>
    <h2 class="headline"><?php print $hiroshige_node_link; ?></h2>
    <?php endif; ?>
    <div class="content">
    <?php if (!empty($submitted)): ?>
    <div class="node-author"><?php print $hiroshige_posted_by; ?></div>
    <?php endif;
    if (!empty($picture)) { print $picture; }
    print $content; ?>
    </div>
    <div class="clear"></div>
    <div class="meta">
      <?php print $links; ?>
    </div>
  <div class="clear"></div>
</div> <!-- node-<?php print $node->nid; ?> -->
