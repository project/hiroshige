<?php
/**
 * @file maintenance-page.tpl.php
 * Provides the maintenance notice page.
 */
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
    <!--[if IE]>
        <?php print phptemplate_get_ie_styles() . "\n"; ?>
    <![endif]-->
  </head>
  <body class="<?php print $body_classes; ?>">
    <div id="header-band" class="clear-block">
        <div id="header" class="container_16">
            <?php if (isset($logo)): ?>
            <a href="<?php print check_url($front_page); ?>" title="<?php print check_plain($site_name); ?>"><img src="<?php print check_url($logo); ?>" alt="<?php print $site_title; ?>" id="logo" /></a>
            <?php endif; ?>
            <div id="title-wrapper">
                <h1><a href="<?php print check_url($front_page); ?>" title="<?php print check_plain($site_name); ?>"><?php print check_plain($site_name); ?></a></h1>
                <?php if (isset($site_slogan)): ?>
                <div id="site-slogan"><?php print check_plain($site_slogan); ?></div>
                <?php endif; ?>
            </div> <!-- /#title-wrapper -->
            <div id="search-top" class="grid_4 omega"><?php if ($search_box) { print $search_box; } ?></div>
        </div> <!-- /#header -->
    </div> <!-- /#header-band -->
    <div id="sub-header-band" class="clear-block">
        <div id="sub-header-container" class="container_16">
            <div id="sub-header" class="grid_16">&nbsp;</div>
        </div>
    </div> <!-- /#sub-header-band -->
    <div id="navigation-band" class="clear-block">
        <div id="primary-links" class="container_16">
            <?php if (isset($primary_links)) : ?>
            <?php print theme('links', $primary_links, array('id' => 'topLinks', 'class' => 'grid_16')) ?>
            <?php endif; ?>
        </div>
    </div> <!-- /#navigation-band -->
    <div id="page-content-band" class="clear-block">
        <div id="content-wrapper" class="container_16">
            <div id="page-content" class="grid_16 alpha omega">
                <?php if (!empty($title)): ?>
                <div id="page-title">
                    <h2><?php print $title; ?></h2>
                </div><!-- #page-title -->
                <div class="clear"></div>
                <?php endif; ?>
                <?php if ($show_messages && $messages): print $messages; endif; ?>
                <?php print $help; ?>
                <div id="content-content" class="clear-block">
                  <?php print $content ?>
                </div> <!-- /#content -->
            </div> <!-- /#page-content -->
        </div> <!-- #content-wrapper" -->
    </div> <!-- /#page-content-band -->
    <div id="bottom-bar-band" class="clear-block">
        <div id="bottom" class="clear-block">
          <div id="bottom-bar" class="container_16"><br/><br/></div>
        </div>
    </div> <!-- /#bottom-bar-band -->
    <div id="footer-band" class="clear-block">
      <div id="footer" class="container_16">
        <?php print $footer_message . $footer ?>
      </div>
    </div> <!-- /#footer-band -->
    <?php print $closure; ?>
  </body>
</html>

