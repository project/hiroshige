<?php
/**
 * @file template.php
 * The Hiroshige theme.
 */

/** The maximum number of elements in the layout grid. */
define('HIROSHIGE_GRID_MAX', 16);

/**
 * Return the path to the main Hiroshige theme directory.
 */
function path_to_hiroshige_theme() {
  static $theme_path;
  
  if (!isset($theme_path)) {
    global $theme;
    if ($theme == 'hiroshige') {
      $theme_path = path_to_theme();
    }
    else {
      $theme_path = drupal_get_path('theme', 'hiroshige');
    }
  }
  return $theme_path;
}


/**
 * Implementation of hook_theme().
 *
 * @param array  $existing
 * @param string $type
 * @param string $theme
 * @param string $path
 *
 * @return array
 */
function hiroshige_theme($existing, $type, $theme, $path) {
  $funcs = array(
    'hiroshige_pager' => array(
      'arguments' => $existing['theme_pager'],
    ),
  );

  return $funcs;
}


function hiroshige_pager($tags = array(), $limit = 10, $element = 0, 
                         $parameters = array(), $quantity = 5) {
  return theme_pager($tags, $limit, $element, $parameters, 5);
}



/**
 * Prepare the user pictures for rendering.
 *
 * @param &$variables array The associative array of template arguments.
 */
function hiroshige_preprocess_user_picture(&$variables) {
  $variables['picture'] = '';

  if (variable_get('user_pictures', 0)) {
    $account = $variables['account'];
    if (!empty($account->picture) && file_exists($account->picture)) {
      $picture = file_create_url($account->picture);
    }
    else if (variable_get('user_picture_default', '')) {
      $picture = variable_get('user_picture_default', '');
    }
    else {
      $picture = path_to_hiroshige_theme() . '/user-icon.png';
    }

    if (isset($picture)) {
      $name = $account->name ? $account->name : variable_get('anonymous', t('Anonymous'));
      $alt  = t("@user's picture", array('@user' => $name));
      $attr = array('class' => 'user-picture');
      $variables['picture'] = theme('image', $picture, $alt, $alt, $attr, FALSE);
      
      // Link the picture if allowed.
      if (!empty($account->uid) && user_access('access user profiles')) {
        $attributes = array('attributes' => array('title' => t('View user profile.')), 'html' => TRUE);
        $variables['picture'] = l($variables['picture'], "user/$account->uid", $attributes);
      }
    }
  }
}


/**
 * Allow themable wrapping of all comments.
 *
 * @param $content string The comments to wrap.
 * @param $node object The node the comments belong to.
 *
 * @return string The rendered HTML.
 */
function phptemplate_comment_wrapper($content, $node) {
  if (!$content || $node->type == 'forum') {
    return '<div id="comments">'. $content .'</div>';
  }
  else {
    return '<div id="comments"><h2 class="comments">'. t('Comments') .'</h2>'. $content .'</div>';
  }
}


/**
 *
 * @param $regions array
 * @param $defaults array
 * @param $direction string
 * @param $vars array
 *
 * @return array
 */
function hiroshige_collapse_regions($vars, $regions, $defaults, $direction = 'right') {
  //--------------------------------------------------------------------------
  // Build a list of regions and their initial widths. Empty regions have a
  // width of zero. If none of the regions have content, bail out.
  $widths = array();

  foreach ($regions as $region) {
    $widths[$region] = empty($vars[$region]) ? 0 : $defaults[$region];
  }

  if (array_sum($widths) == 0) {
    return array();
  }

  if ($direction == 'left') {
    $regions = array_reverse($regions);
  }

  //--------------------------------------------------------------------------
  // If the first region is empty, shift until we find the first non-empty
  // one.
  $prev = array_shift($regions);
  while ($widths[$prev] == 0) {
    $prev = array_shift($regions);
  }

  $widths[$prev] = $defaults[$prev];

  //--------------------------------------------------------------------------
  // Collapse the empty ones.
  foreach ($regions as $region) {
    if ($widths[$region] == 0) {
        $widths[$prev] += $defaults[$region];
    }
    else {
        $prev = $region;
    }
  }

  //--------------------------------------------------------------------------
  // After collapsing, the total of the widths should be 16. If it isn't the
  // extra space is added to the last region in the list to get the correct
  // page width.
  $widths  = array_filter($widths);
  $regions = array_keys($widths);
  $total   = array_sum($widths);
  
  if ($total < HIROSHIGE_GRID_MAX) {
    $last = end($regions);
    $widths[$last] += (HIROSHIGE_GRID_MAX - $total);
  }

  //--------------------------------------------------------------------------
  // Build the classes.
  $classes = array();
  if (count($widths) > 1) {
    for ($i = 0; $i < count($regions); $i++) {
      $region = $regions[$i];
      $classes[$region] = 'grid_' . $widths[$region]; 

      if ($i == 0) {
        $classes[$region] .= ' alpha';
      }
      else if ($i == (count($regions) - 1)) {
        $classes[$region] .= ' omega';
      }
    }
  }
  else {
    $classes[$regions[0]] = 'grid_16';
  }

  return $classes;
}


/**
 * Override or insert PHPTemplate variables into the templates.
 */
function phptemplate_preprocess_page(&$vars) {
  $vars['tabs2']         = menu_secondary_local_tasks();

  // The default widths of the theme's regions.
  $default_widths = array(
    'sub_head_left'         => 4,
    'sub_head_center_left'  => 4,
    'sub_head_center_right' => 4,
    'sub_head_right'        => 4,
    'content'               => 8,
    'left'                  => 4,
    'right'                 => 4,
    'bottom_left'           => 4,
    'bottom_center_left'    => 4,
    'bottom_center_right'   => 4,
    'bottom_right'          => 4,
    'footer'                => 16,
  );
  
  $classes               = array();

  //--------------------------------------------------------------------------
  // Determine if the page title CSS classes.
  $title_class = array();
  $inkline     = TRUE;

  if ($vars['node']) {
    $show_info = theme_get_setting('toggle_node_info_'. $vars['node']->type);
    if (!(boolean)$show_info) {
        $title_class[] = 'inkline';
    }
  }
  else {
    $title_class[] = 'inkline';
  }

  if (!empty($vars['tabs'])) {
    $title_class[] = 'with-tabs';
  }

  $title_class = trim(implode(' ', $title_class));
  $title_class = (!empty($title_class)) ? ' class="' . $title_class . '" ' : '';

  $classes['page_title'] = $title_class;

  //--------------------------------------------------------------------------
  // Compute the classes for the sub-header block regions.
  $regions   = array(
    'sub_head_left',
    'sub_head_center_left',
    'sub_head_center_right',
    'sub_head_right',
  );
  $collapse  = theme_get_setting('hiroshige_header_expansion');
  $direction = empty($collapse) ? 'right' : $collapse;
  $classes   = array_merge($classes, hiroshige_collapse_regions($vars, $regions, $default_widths, $direction));

  //--------------------------------------------------------------------------
  // Compute the classes for the middle content regions.
  $regions   = array(
    'content',
    'left',
    'right',
  );
  $collapse  = theme_get_setting('hiroshige_content_expansion');
  $direction = empty($collapse) ? 'right' : $collapse;
  $classes   = array_merge($classes, hiroshige_collapse_regions($vars, $regions, $default_widths, $direction));

  //--------------------------------------------------------------------------
  // Compute the classes for the block regions at the bottom.
  $regions   = array(
    'bottom_left',
    'bottom_center_left',
    'bottom_center_right',
    'bottom_right',
  );
  $collapse  = theme_get_setting('hiroshige_bottom_expansion');
  $direction = empty($collapse) ? 'right' : $collapse;
  $classes   = array_merge($classes, hiroshige_collapse_regions($vars, $regions, $default_widths, $direction));

  $vars['hiroshige_classes'] = $classes;
}


/**
 * Add some variables for the node template.
 *
 * @param &$vars array The variables array.
 */
function phptemplate_preprocess_node(&$vars) {
  $vars['hiroshige_node_class'] = 'node ' . 
                                  ($vars['sticky'] ? ' sticky ' : '') . 
                                  ($vars['status'] ? '' : ' node-unpublished') . 
                                  ' node-' . $vars['node']->type . ' ' . 
                                  ($vars['teaser'] ? 'teaser' : '');

  $perma = t('Permanent Link to !title', array('!title' => $vars['title']));                                  
  $vars['hiroshige_node_link'] = '<a href="' . $vars['node_url'] . '" ' . 
                                 'rel="bookmark" title="' . $perma . '">' . 
                                 $vars['title'] . '</a>';                                    

  //--------------------------------------------------------------------------
  // Render the term list.
  if (!empty($vars['taxonomy'])) {
    $links = array();

    foreach ($vars['taxonomy'] as $link) {
      $links[] = l($link['title'], $link['href'], $link);
    }

    $term_list = implode(', ', $links);
  }
  else {
    $term_list = '';
  }

  //--------------------------------------------------------------------------
  // Put it all together.
  $subs = array('!name'  => $vars['name'], 
                '@date'  => format_date($vars['created'], 'custom', 'F jS, Y'),
                '!terms' => $term_list);         
  if (!empty($term_list)) {
    $vars['hiroshige_posted_by'] = t('Posted on @date by !name and tagged !terms.', $subs);
  }
  else {
    $vars['hiroshige_posted_by'] = t('Posted on @date by !name.', $subs);
  }
}


/**
 * Overrides template_preprocess_comment().
 *
 * @param array $variables
 */
function phptemplate_preprocess_comment(&$variables) {
  $comment = $variables['comment'];
  $node    = $variables['node'];

  $variables['author']  = theme('username', $comment);
  $variables['content'] = $comment->comment;

  $params = array(
    '@date' => format_date($comment->timestamp, 'custom', 'M jS, Y'),
    '@time' => format_date($comment->timestamp, 'custom', 'g:i a'),
  );
  $variables['date']    = t('@date at @time', $params);
}


/**
 * Returns the rendered local tasks. The default implementation renders
 * them as tabs. Overridden to split the secondary tasks.
 *
 * @ingroup themeable
 */
function phptemplate_menu_local_tasks() {
  return menu_primary_local_tasks();
}


/**
 * Generates IE CSS links for LTR and RTL languages.
 *
 * @return string the IE style elements.
 */
function phptemplate_get_ie_styles() {
  global $language;

  $iecss = '<link type="text/css" rel="stylesheet" media="screen" href="'. base_path() . path_to_theme() .'/fix-ie.css" />';
  if (defined('LANGUAGE_RTL') && $language->direction == LANGUAGE_RTL) {
    $iecss .= '<style type="text/css" media="screen">@import "'. base_path() . path_to_theme() .'/fix-ie-rtl.css";</style>';
  }

  return $iecss;
}

