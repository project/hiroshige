<?php
/**
 * @file theme-settings.php
 * Provides settings for the Hiroshige theme.
 */

/**
 * Implementation of THEMEHOOK_settings().
 *
 * @param array $settings An array of saved settings for this
 *        theme.
 *
 * @return array A form array.
 */
function phptemplate_settings($settings) {
  $form = array();

  $description = t('Choose "Left to Right" to cause non-empty regions to expand to fill the space allocated for any empty regions to their right; choose "Right to Left" to fill empty space in the opposite direction.');

  //--------------------------------------------------------------------------
  // Which direction should empty blocks be expanded in the sub-header block
  // region?
  $current = empty($settings['hiroshige_header_expansion']) ? 
             'right' : $settings['hiroshige_header_expansion'];
  $options = array('right' => t('Left to Right'), 'left' => t('Right to Left'));
  $form['hiroshige_header_expansion'] = array(
    '#type'          => 'select',
    '#title'         => t('Header Expansion Direction'),
    '#options'       => $options,
    '#default_value' => $current,
    '#description'   => $description,
  );
  
  //--------------------------------------------------------------------------
  // Which direction should empty blocks be expanded in the bottom block
  // region?
  $current = empty($settings['hiroshige_content_expansion']) ?
             'right' : $settings['hiroshige_content_expansion'];
  $options = array('right' => t('Left to Right'), 'left' => t('Right to Left'));
  $form['hiroshige_content_expansion'] = array(
    '#type'          => 'select',
    '#title'         => t('Content Expansion Direction'),
    '#options'       => $options,
    '#default_value' => $current,
    '#description'   => $description,
  );
  
  //--------------------------------------------------------------------------
  // Which direction should empty blocks be expanded in the bottom block
  // region?
  $current = empty($settings['hiroshige_bottom_expansion']) ?
             'right' : $settings['hiroshige_bottom_expansion'];
  $options = array('right' => t('Left to Right'), 'left' => t('Right to Left'));
  $form['hiroshige_bottom_expansion'] = array(
    '#type'          => 'select',
    '#title'         => t('Bottom Expansion Direction'),
    '#options'       => $options,
    '#default_value' => $current,
    '#description'   => $description,
  );

  return $form;
}
